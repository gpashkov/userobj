#pragma once
#include "StackWalker.h"

class StackWalkerEx : public StackWalker {
public:
	StackWalkerEx() : StackWalker() {

	}

	//! TODO: Should be thread-local!!!
	static StackWalkerEx *getInstance() {
		static StackWalkerEx *instance;
		if (!instance) {
			instance = new StackWalkerEx();
			instance->LoadModules();
		}

		return instance;
	}

	using WalkerFunc = std::function<void(CallstackEntryType, CallstackEntry &)>;

	//! Walk the stack, invoking the callback on each entry
	void walk(const WalkerFunc &walker) {
		m_currentWalker = &walker;
		ShowCallstack();
		m_currentWalker = nullptr;
	}

	virtual void OnCallstackEntry(CallstackEntryType eType, CallstackEntry &entry) {
		if (m_currentWalker) {
			(*m_currentWalker)(eType, entry);
		}
#ifdef _DEBUG
		//StackWalker::OnCallstackEntry(eType, entry);
#endif
	}

private:
	WalkerFunc const *m_currentWalker;
};
