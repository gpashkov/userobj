#pragma once
class Tracker;
class Logger;

class JsonDumper
{
public:
	JsonDumper(Logger &logger);
	~JsonDumper();

	void dump(Tracker &tracker, const std::string &fileName);

private:
	Logger &m_logger;
};

