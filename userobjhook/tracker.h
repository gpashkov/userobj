#pragma once

#include "stdafx.h"
#include "StackWalkerEx.h"
#include <sstream>
#include <algorithm>
#include <utility>
#include <ctime>
#include <mutex>

using namespace std;

class Logger {
public:
	virtual void log(string &&msg) = 0;

	template<typename ...Args>
	void log(Args&&... args) {
		stringstream stream;
		toStream(stream, args...);
		log(stream.str());
	}

private:
	template<typename T, typename ...Args>
	void toStream(stringstream &stream, T&& t, Args&&... args) {
		stream << t;
		toStream(stream, args...);
	}

	void toStream(stringstream &stream) {}
};

class DebugOutputLogger : public Logger {
	virtual void log(std::string &&msg) override {
		OutputDebugStringA(msg.c_str());
	}
};

class CallStack {
public:
	// todo: implement
	static CallStack current() {
		auto current = CallStack();
		auto walker = StackWalkerEx::getInstance();
		walker->walk([&current](StackWalkerEx::CallstackEntryType type, StackWalkerEx::CallstackEntry &entry) {
			//TODO: Can we avoid copy by refactoring StackWalker to use move semantics?
			if (type != StackWalkerEx::lastEntry) // last entry is already reported as nextEntry
				current.m_entries.push_back(entry);
		});

		// Keep top frame at the end
		//std::reverse(std::begin(current.m_entries), std::end(current.m_entries));

		return current;
	}

	bool isEmpty() const { return m_entries.empty(); }

	//! Remove the top element of the callstack (useful when creating stack from an utility function)
	void pop() {
		m_entries.pop_back();
	}

	std::vector<std::string> format() const {
		std::vector<std::string> formatted;
		formatted.reserve(m_entries.size());
		for (const auto &frame : m_entries) {
			stringstream ss;
			ss << frame.moduleName << "!" << frame.name;
			formatted.push_back(ss.str());
		}
		return formatted;
	}

private:
	std::vector<StackWalkerEx::CallstackEntry> m_entries;
};

enum class ObjType : int{
	Window = 0,
	Dialog,
	Menu,
	Icon,
	Cursor,
	_Last
};

inline std::string objTypeToString(ObjType ot) {
	static std::vector<std::string> names = {
		"Window",
		"Dialog",
		"Menu",
		"Icon",
		"Cursor"
	};
	if ((int)ot >= 0 && (int)ot < (int)ObjType::_Last)
		return names[(size_t)ot];
	return "<Unknown>";
}

struct TrackedObj {
	void *handle;
	ObjType type;
	CallStack creationStack;
	CallStack deletionStack;
	time_t creationTime;
	time_t deletionTime;
};

class Tracker {
public:
	Tracker(Logger &logger)
		: m_logger(logger)
	{}

	Logger &getLogger() { return m_logger;  }

	void add(void *obj, ObjType type){
		std::lock_guard<std::mutex> g(m_mutex);
		m_objects.insert(make_pair(obj, TrackedObj{ obj, type, CallStack::current(), CallStack(), time(0), 0 }));
	}

	void markDestroyed(void *obj) {
		std::lock_guard<std::mutex> g(m_mutex);

		std::vector<TrackedObj*> candidates;

		auto matches = m_objects.equal_range(obj);
		for (auto it = matches.first; it != matches.second; ++it) {
			auto &trackedObj = it->second;
			if (trackedObj.deletionStack.isEmpty()) {
				candidates.push_back(&trackedObj);
			}
		}

		if (candidates.empty()) {
			m_logger.log("Untracked object is being destroyed: ", obj);
			return;
		}

		if (candidates.size() > 1) {
			m_logger.log("More than 1 tracked object matches the handle being destroyed. All will be marked.");
		}

		auto stack = CallStack::current();
		auto timestamp = time(0);

		for (auto &t : candidates) {
			t->deletionStack = stack;
			t->deletionTime = timestamp;
		}
	}

	void foreach(const std::function<void(const TrackedObj &)> &visitor) {
		std::lock_guard<std::mutex> g(m_mutex);
		for (const auto &p : m_objects) {
			visitor(p.second);
		}
	}

private:
	Logger &m_logger;
	std::mutex m_mutex;
	unordered_multimap<void*, TrackedObj> m_objects;
};