#include "stdafx.h"
#include "JsonDumper.h"
#include "tracker.h"
#include "json.hpp"
#include <fstream>
#include <ctime>

using json = nlohmann::json;

JsonDumper::JsonDumper(Logger &logger)
	: m_logger(logger)
{
}

JsonDumper::~JsonDumper()
{
}

void JsonDumper::dump(Tracker &tracker, const std::string & fileName) {
	json j;
	j["version"] = "1.0";
	j["pid"] = GetCurrentProcessId();
	j["timeStamp"] = time(0);
	auto objects = json::array();

	tracker.foreach([&objects] (const TrackedObj &to){
		objects.push_back({
			{"type", objTypeToString(to.type)},
			{"creationTime", to.creationTime},
			{"deletionTime", to.deletionTime},
			{"creationStack", to.creationStack.format() },
			{"deletionStack", to.deletionStack.format() }
		});
	});

	j["objects"] = std::move(objects);

	std::ofstream os(fileName);
	if (!os.good()) {
		m_logger.log("Unable to write into file: ", fileName);
		return;
	}

	os << j;
	os.close();
}
