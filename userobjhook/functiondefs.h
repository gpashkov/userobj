#pragma once
#include <Windows.h>


// WinAPI functions that are to be hooked

/*

WINUSERAPI
HWND
WINAPI
CreateWindowExW(
	_In_ DWORD dwExStyle,
	_In_opt_ LPCWSTR lpClassName,
	_In_opt_ LPCWSTR lpWindowName,
	_In_ DWORD dwStyle,
	_In_ int X,
	_In_ int Y,
	_In_ int nWidth,
	_In_ int nHeight,
	_In_opt_ HWND hWndParent,
	_In_opt_ HMENU hMenu,
	_In_opt_ HINSTANCE hInstance,
	_In_opt_ LPVOID lpParam);
	*/
typedef HWND(WINAPI *CREATEWINDOWEXW)(DWORD, LPCWSTR, LPCWSTR, DWORD, int, int, int, int, HWND, HMENU, HINSTANCE, LPVOID);

/*
WINUSERAPI BOOL WINAPI DestroyWindow( _In_ HWND hWnd);
*/
typedef BOOL(WINAPI *DESTROYWINDOW)(HWND);

/*
WINUSERAPI HWND WINAPI CreateDialogParamW(
_In_opt_ HINSTANCE hInstance,
_In_ LPCWSTR lpTemplateName,
_In_opt_ HWND hWndParent,
r_In_opt_ DLGPROC lpDialogFunc,
_In_ LPARAM dwInitParam);
*/
typedef HWND(WINAPI *CREATEDIALOGPARAMW)(HINSTANCE,LPCWSTR,HWND,DLGPROC,LPARAM);

/*
WINUSERAPI HWND WINAPI CreateDialogIndirectParamW(
	_In_opt_ HINSTANCE hInstance,
	_In_ LPCDLGTEMPLATEW lpTemplate,
	_In_opt_ HWND hWndParent,
	_In_opt_ DLGPROC lpDialogFunc,
	_In_ LPARAM dwInitParam);
*/

/*
WINUSERAPI HCURSOR WINAPI CreateCursor(
    _In_opt_ HINSTANCE hInst,
    _In_ int xHotSpot,
    _In_ int yHotSpot,
    _In_ int nWidth,
    _In_ int nHeight,
    _In_ CONST VOID *pvANDPlane,
    _In_ CONST VOID *pvXORPlane);

WINUSERAPI BOOL WINAPI DestroyCursor(_In_ HCURSOR hCursor);
*/
typedef HCURSOR(WINAPI *CREATECURSOR)(HINSTANCE,int,int,int,int,CONST VOID*,CONST VOID*);

/*
WINUSERAPI BOOL WINAPI DestroyCursor(_In_ HCURSOR hCursor);
*/
typedef BOOL(WINAPI *DESTROYCURSOR)(HCURSOR);

/*
WINUSERAPI HCURSOR WINAPI LoadCursorW( _In_opt_ HINSTANCE hInstance, _In_ LPCWSTR lpCursorName);
*/
typedef HCURSOR(WINAPI *LOADCURSORW)(HINSTANCE,LPCWSTR);

/*
WINUSERAPI HCURSOR WINAPI LoadCursorFromFileW(_In_ LPCWSTR lpFileName);
*/
typedef HCURSOR(WINAPI *LOADCURSORFROMFILEW)(LPCWSTR);

/*
WINUSERAPI HANDLE WINAPI LoadImageW(
	_In_opt_ HINSTANCE hInst,
	_In_ LPCWSTR name,
	_In_ UINT type,
	_In_ int cx,
	_In_ int cy,
	_In_ UINT fuLoad);
*/
typedef HANDLE(WINAPI *LOADIMAGEW)(HINSTANCE,LPCWSTR,UINT,int,int,UINT);

/*
WINUSERAPI HANDLE WINAPI CopyImage(
    _In_ HANDLE h,
    _In_ UINT type,
    _In_ int cx,
    _In_ int cy,
    _In_ UINT flags);
*/
typedef HANDLE(WINAPI *COPYIMAGE)(HANDLE,UINT,int,int cy,UINT);

/*
WINUSERAPI HMENU WINAPI CreateMenu(VOID);
WINUSERAPI HMENU WINAPI CreatePopupMenu(VOID);
WINUSERAPI HMENU WINAPI LoadMenuW( _In_opt_ HINSTANCE hInstance, _In_ LPCWSTR lpMenuName);
WINUSERAPI HMENU WINAPI LoadMenuIndirectW(_In_ CONST MENUTEMPLATEW *lpMenuTemplate);
WINUSERAPI BOOL WINAPI DestroyMenu( _In_ HMENU hMenu); 
*/
typedef HMENU (WINAPI *CREATEMENU)(VOID);
typedef HMENU (WINAPI *CREATEPOPUPMENU)(VOID);
typedef HMENU (WINAPI *LOADMENUW)(HINSTANCE, LPCWSTR);
typedef HMENU (WINAPI *LOADMENUINDIRECTW)(CONST MENUTEMPLATEW *);
typedef BOOL (WINAPI *DESTROYMENU)(HMENU); 


/*!
Not supported:
DDE conversation	DdeConnect, DdeConnectList	DdeDisconnect, DdeDisconnectList
Hook				SetWindowsHookEx	UnhookWindowsHookEx
CreateMDIWindow
Window position	BeginDeferWindowPos	EndDeferWindowPos
Accelerator table	CreateAcceleratorTable	DestroyAcceleratorTable
Caret	CreateCaret	DestroyCaret
DDE conversation	DdeConnect, DdeConnectList	DdeDisconnect, DdeDisconnectList
*/

/*
User object	Creator function	Destroyer function
Accelerator table	CreateAcceleratorTable	DestroyAcceleratorTable
Cursor	CreateCursor, LoadCursor, LoadImage	DestroyCursor
Icon	CreateIconIndirect, LoadIcon, LoadImage	DestroyIcon
Menu	CreateMenu, CreatePopupMenu, LoadMenu, LoadMenuIndirect	DestroyMenu
Window	CreateWindow, CreateWindowEx, CreateDialogParam, CreateDialogIndirectParam, CreateMDIWindow	DestroyWindow
Window position	BeginDeferWindowPos	EndDeferWindowPos
*/