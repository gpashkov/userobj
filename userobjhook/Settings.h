#pragma once

#include <ShlObj.h>
#include <fstream>
#include "json.hpp"

/*! WARNING: this class is shared between the DLL and the main app:
		- Keep it header-only
		- Ensure it has no non-library dependencies
		- OR make it properly exported, etc.
	I know it's a shitty approach, but proper dependency management is out of scope now.
*/
class Settings {
public:
	const std::string &getSymPath() const { return m_sympath; }
	const std::string &getOutputDir() const { return m_outputDir; }
	const std::string &getDumpEventName() const { return m_dumpEventName; }

	bool isValid() const { return m_error.empty(); }
	const std::string &getError() const { return m_error; }

	static Settings *getInstance() {
		static Settings *instance;
		if (!instance)
			instance = new Settings();

		return instance;
	}

private:
	static std::string getHomeDir() {
		char path[MAX_PATH];
		if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_PROFILE, NULL, 0, path))) {
			return std::string(path);
		}
		return std::string();
	}

	Settings() {
		using json = nlohmann::json;
		auto homeDir = getHomeDir();
		auto settingsFile = homeDir + "\\userobjhook.json";
		std::fstream fs(settingsFile);
		json js;
		try {
			while (fs.good()) {
				fs >> js;
			}

			m_sympath = js.value("sympath", "");
			m_outputDir = js.value("output-dir", "");
			m_dumpEventName = js.value("dump-event-name", "");
		}
		catch (std::exception &ex) {
			OutputDebugStringA("Exception while loading settings from JSON");
			OutputDebugStringA(ex.what());
			m_error = ex.what();
		}
		if (m_outputDir.empty()) {
			m_outputDir = homeDir;
		}
	}

	std::string m_sympath;
	std::string m_outputDir;
	std::string m_dumpEventName;
	
	std::string m_error;
};
