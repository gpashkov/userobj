// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "tracker.h"
#include "functiondefs.h"
#include "Settings.h"
#include "JsonDumper.h"
#include <MinHook.h>
#include <thread>
#include <ctime>

static Logger *mainLogger;
static Tracker *tracker;

// Pointers to original functions
static CREATEWINDOWEXW Orig_CreateWindowExW; 
static DESTROYWINDOW   Orig_DestroyWindow;

template<typename Fn, Fn fn, typename... Args>
typename std::result_of<Fn(Args...)>::type
wrapper(Args&&... args) {
	return fn(std::forward<Args>(args)...);
}
//#define WRAPPER(FUNC) wrapper<decltype(&FUNC), &FUNC>


/*
template<typename Fn, Fn *fn>
struct HookedFunc {

	HookedFunc(const char *name, const ObjType objType)
		:name(name)
		,type(objType)
	{
		wrapper<decltype(fn), fn>((HINSTANCE)nullptr, (LPCWSTR)nullptr);
		auto x = &wrapper<decltype(fn), fn>;

		if (MH_CreateHook(fn, (LPVOID)(Fn*)&wrapper<decltype(fn), fn>,
			reinterpret_cast<LPVOID*>(&original)) != MH_OK) {
		}
	}

	Fn *original;
	const char *name;
	const ObjType type;
};
*/
//
//template<typename Fn, Fn *fn, typename...Args>
//auto makeHook() -> HookedFunc2<Fn, fn, typename std::result_of<Fn(Args...)>::type, Args> {
//	return HookedFunc2<Fn, fn, std::result_of<Fn(Args...)>::type, Args...>();
//}

template<typename T>
struct function_traits;

template<typename R, typename ...Args>
struct function_traits<std::function<R(Args...)>>
{
	static const size_t nargs = sizeof...(Args);

	typedef typename R result_type;

	template <size_t i>
	struct arg
	{
		typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
	};
};

struct HookedFuncBase {
	HookedFuncBase(const char *name, Tracker &t)
		: m_tracker(t)
		, m_name(name)
	{
	}

	const char *m_name;
	Tracker &m_tracker;

	virtual ~HookedFuncBase() {}
};

enum FuncKind {
	Creates,
	Destroys
};

template<typename Fn, Fn fn, typename...Args>
struct HookedFunc : HookedFuncBase {
	using FuncType = decltype(fn);
	using Self = HookedFunc<Fn, fn, Args...>;

	HookedFunc(const char *name, Tracker &t, FuncKind kind, ObjType objType)
		: HookedFuncBase(name, t)
		, m_kind(kind)
		, m_objType(objType)
	{
		assert(s_instance == nullptr);
		s_instance = this;

		if (MH_CreateHook(fn, &wrapper,
			reinterpret_cast<LPVOID*>(&m_original)) != MH_OK) {
			mainLogger->log("MH_CreateHook failed for CreateWindowExW");
		}
	}

	virtual ~HookedFunc() {
		MH_DisableHook(fn);
		MH_RemoveHook(fn);
	}

	//! Only a single instance per template instantiation will get created
	static Self *s_instance;

	FuncType m_original;
	FuncKind m_kind; // Ideally this should be a template argument
	ObjType m_objType;

	template<typename First, typename...Others>
	static auto logDestruction(First obj, Others...) {
		s_instance->m_tracker.markDestroyed((void*)obj);
	}

	static auto logDestruction() {}
	
	static auto wrapper(Args... args) -> typename std::result_of<Fn(Args...)>::type {
		auto &logger = s_instance->m_tracker.getLogger();
		logger.log("In hooked ", s_instance->m_name);

		auto result = s_instance->m_original(args...);
		if (result) {
			if (s_instance->m_kind == FuncKind::Creates) {
				// Warning here is expected for FuncKind::Destroys
				s_instance->m_tracker.add((void*)result, s_instance->m_objType);
			}
			else {
				// All functions that destroy: first argument is the handle
				logDestruction(args...);
			}
		} else {
			logger.log("Hooked function returned 0");
		}
		return result;
	}
};

template<typename Fn, Fn fn, typename...Args>
typename HookedFunc<Fn, fn, Args...>* HookedFunc<Fn, fn, Args...>::s_instance;

static std::vector<std::unique_ptr<HookedFuncBase>> allFunctions;

template<typename Fn, Fn fn, typename...Args>
std::unique_ptr<HookedFunc<Fn, fn, Args...>>
hook(const char *name, FuncKind kind, ObjType type)
{
	return std::make_unique<HookedFunc<Fn, fn, Args...>>(name, *tracker, kind, type);
}

void createHooks() {

	// Windowing functions
	allFunctions.push_back(
		hook<decltype(&CreateWindowExW), &CreateWindowExW, DWORD, LPCWSTR,
		LPCWSTR, DWORD, int, int, int, int, HWND, HMENU, HINSTANCE, LPVOID>
		("CreateWindowExW", FuncKind::Creates, ObjType::Window));

	allFunctions.push_back(
		hook<decltype(&DestroyWindow), &DestroyWindow, HWND>
		("DestroyWindow", FuncKind::Destroys, ObjType::Window));

	// Menu functions
	allFunctions.push_back(
		hook<decltype(&LoadMenuW), &LoadMenuW, HINSTANCE, LPCWSTR>
		("LoadMenuW", FuncKind::Creates, ObjType::Menu));

	allFunctions.push_back(
		hook<decltype(&LoadMenuIndirectW), &LoadMenuIndirectW, CONST MENUTEMPLATEW*>
		("LoadMenuIndirectW", FuncKind::Creates, ObjType::Menu));

	allFunctions.push_back(
		hook<decltype(&CreateMenu), &CreateMenu>
		("CreateMenu", FuncKind::Creates, ObjType::Menu));

	allFunctions.push_back(
		hook<decltype(&CreatePopupMenu), &CreatePopupMenu>
		("CreatePopupMenu", FuncKind::Creates, ObjType::Menu));


	allFunctions.push_back(
		hook<decltype(&DestroyMenu), &DestroyMenu, HMENU>
		("DestroyMenu", FuncKind::Destroys, ObjType::Menu));

	// Dialog functions
	allFunctions.push_back(
		hook<decltype(&CreateDialogParamW), &CreateDialogParamW,
		HINSTANCE, LPCWSTR, HWND, DLGPROC, LPARAM>
		("CreateDialogParamW", FuncKind::Creates, ObjType::Dialog)
		);

	allFunctions.push_back(
		hook<decltype(&CreateDialogIndirectParamW), &CreateDialogIndirectParamW,
		HINSTANCE, LPCDLGTEMPLATEW, HWND, DLGPROC, LPARAM>
		("CreateDialogIndirectParamW", FuncKind::Creates, ObjType::Dialog));

	// Cursor functions
	allFunctions.push_back(
		hook<decltype(&CreateCursor), &CreateCursor, HINSTANCE,int,int,int,int,
		const void*,const void*>
		("CrateCursor", FuncKind::Creates, ObjType::Cursor));

	allFunctions.push_back(
		hook<decltype(&LoadCursorW), &LoadCursorW, HINSTANCE, LPCWSTR>
		("LoadCursorW", FuncKind::Creates, ObjType::Cursor));

	allFunctions.push_back(
		hook<decltype(&LoadCursorFromFileW), &LoadCursorFromFileW, LPCWSTR>
		("LoadCursorFromFileW", FuncKind::Creates, ObjType::Cursor));

	allFunctions.push_back(
		hook<decltype(&DestroyCursor), &DestroyCursor, HCURSOR>
		("DestroyCursor", FuncKind::Destroys, ObjType::Cursor));
}

void removeHooks() {

	// Unnecessary (hook destructors will do this), but just to be sure
	MH_DisableHook(MH_ALL_HOOKS);

	// hook destructor will do the job
	allFunctions.clear();
}

//! Create and enable API hooks
void setupHooks() {
	mainLogger->log("Setting up hooks...");

	createHooks();

	// Hooks are created in disabled state. Enable them all.
	if (MH_EnableHook(MH_ALL_HOOKS) != MH_OK) {
		mainLogger->log("MH_EnableHook failed for MH_ALL_HOOKS");
	}

}

void createEventThread() {
	mainLogger->log("Creating the event thread...");

	srand((unsigned)time(0));

	auto settings = Settings::getInstance();
	auto outputDir = settings->getOutputDir();
	auto eventName = settings->getDumpEventName();
	auto pid = GetCurrentProcessId();

	std::thread([eventName, outputDir, pid]() {
		auto event = OpenEventA(EVENT_ALL_ACCESS, false, eventName.c_str());
		if (!event) {
			mainLogger->log("Unable to open the dump event. Last error: ", GetLastError());
			return;
		}

		while (WaitForSingleObject(event, INFINITE) == WAIT_OBJECT_0) {
			// Event signalled, let's dump it!
			mainLogger->log("Received dump event. Executing!");

			// Try to construct a somewhat unique name
			std::stringstream ssPid;
			ssPid
				<< pid
				<< "-"
				<< unsigned(rand());

			auto outputPath = outputDir + "\\" + ssPid.str() + "-output.json";

			JsonDumper(*mainLogger).dump(*tracker, outputPath);

			SetEvent(event);
			return;
		}
		mainLogger->log("Event thread exiting!");
	}).detach();
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		mainLogger = new DebugOutputLogger();
		tracker = new Tracker(*mainLogger);

		if (MH_Initialize() != MH_OK) {
			mainLogger->log("Failed to MH_Initialize(). Nothing will work.");
			return FALSE;
		}

		setupHooks();

		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
	{
		removeHooks();
		MH_Uninitialize();
	}
		break;
	}

	return TRUE;
}

//! The hook proc used for injecting this DLL with SetWindowsHookEx
extern "C" __declspec(dllexport) int WinHookProc(int code, WPARAM wParam, LPARAM lParam) {

	static bool firstRun = true;

	if (firstRun) {
		firstRun = false;
		createEventThread();
	}

	return (int)CallNextHookEx(NULL, code, wParam, lParam);
}