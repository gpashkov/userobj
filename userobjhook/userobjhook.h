#pragma once

// Public header of the userobjhook DLL

#ifdef USEROBJHOOK_EXPORTS
#	define USEROBJHOOK_API __declspec(dllexport)
#else
#	define USEROBJHOOK_API __declspec(dllimport)
#endif
