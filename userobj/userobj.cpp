// userobj.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <memory>
#include <TlHelp32.h>
#include "Settings.h"

using namespace std;

void printUsage(char *progName);
DWORD getFirstThreadId(DWORD pid);

int main(int argc, char *argv[])
{

	if (argc < 2) {
		printUsage(argv[0]);
		return 0;
	}

	auto settings = Settings::getInstance();
	if (!settings->isValid()) {
		cout << "Settings file is invalid" << endl;
		cout << settings->getError() << endl;
		return 1;
	}

	auto pid = atoi(argv[1]);
	auto tid = getFirstThreadId(pid);
	if (tid == 0) {
		cout << "Unable to get any thread ID from process " << pid << endl;
		cout << "Please ensure that the process id refers to an existing process" << endl;
		return 1;
	}

	const char *hookDll = "userobjhook.dll";
	auto dllHandle = LoadLibraryA(hookDll);
	if (!dllHandle) {
		cout << "Could not load " << hookDll << endl;
		cout << "Last error: " << hex << GetLastError() << endl;
		return 1;
	}

	auto hookProc = GetProcAddress(dllHandle, "WinHookProc");
	if (!hookProc) {
		cout << "Could not locate the hook proc in the hook DLL" << endl;
		cout << "Last error: " << hex << GetLastError() << endl;
		return 1;
	}


	// This event will be opened by the hook DLL. When it's triggered, the data will get dumped. Simple as that.
	auto eventName = settings->getDumpEventName();
	auto evtHandle = CreateEventA(nullptr, false, false, eventName.c_str());
	if (!evtHandle) {
		cout << "CreateEvent failed for the dump event. Event name: " << eventName << endl;
		cout << "Last error: " << hex << GetLastError() << endl;
		return 1;
	}
	auto evtGuard = std::shared_ptr<void>(evtHandle, CloseHandle);

	cout << "Hooking into thread " << tid << endl;

	auto hook = SetWindowsHookEx(WH_CALLWNDPROC, (HOOKPROC)hookProc, dllHandle, tid);
	if (!hook) {
		cout << "Unable to set windows hook." << endl;
		cout << "Last error: " << hex << GetLastError() << endl;
	}

	cout << "Hooked now. Please proceed with your testing." << endl;
	cout << "Press ENTER to stop tracking." << endl;
	getchar();

	if (SetEvent(evtHandle)) {
		cout << "Dump event set!" << endl;
	}
	else {
		cout << "Failed to set dumping event! Last error: " << hex << GetLastError() << endl;
	}

	cout << "Waiting for the process to finish dumping data..." << endl;
	WaitForSingleObject(evtHandle, 60 * 1000);

	//UnhookWindowsHookEx(hook);

    return 0;
}

void printUsage(char *progName) {
	cout << "Usage: " << endl;
	cout << "\t" << progName << " pid" << endl;
	cout << endl;
	cout << "Where pid is identifier of the process to be hooked (decimal)." << endl;
	cout << endl;
	cout << "When things go wrong: " << endl;
	cout << "Some diagnostics output can be viewed in DbgView." << endl;
}

DWORD getFirstThreadId(DWORD pid) {
	const std::shared_ptr<void> hThreadSnapshot(
		CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0), CloseHandle);

	if (hThreadSnapshot.get() == INVALID_HANDLE_VALUE) {
		throw std::runtime_error("GetMainThreadId failed");
	}
	THREADENTRY32 tEntry;
	tEntry.dwSize = sizeof(THREADENTRY32);
	DWORD result = 0;
	for (BOOL success = Thread32First(hThreadSnapshot.get(), &tEntry);
	!result && success && GetLastError() != ERROR_NO_MORE_FILES;
		success = Thread32Next(hThreadSnapshot.get(), &tEntry))
	{
		if (tEntry.th32OwnerProcessID == pid) {
			result = tEntry.th32ThreadID;
		}
	}
	return result;
}